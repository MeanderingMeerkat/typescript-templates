import {Command} from 'commander';
import {z} from 'zod';

import {splitStr} from './split';

const program = new Command();

program.name('string-util').description('CLI to some string utilities');

program
  .command('split')
  .description('Split a string into substrings and display as an array')
  .argument('<string>', 'string to split')
  .option('-s, --separator <char>', 'separator character', ',')
  .action((str, options) => {
    const _str = z.string().nonempty().parse(str);
    const _options = z.object({separator: z.string().length(1)}).parse(options);
    console.log(splitStr(_str, _options.separator));
  });

program.parse();
