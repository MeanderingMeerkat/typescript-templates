export const splitStr = (str: string, separator: string): string[] => {
  return str.split(separator);
};
