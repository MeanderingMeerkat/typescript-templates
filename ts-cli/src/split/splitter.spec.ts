import {splitStr} from './splitter';

describe('String Splitter', () => {
  beforeAll(async () => {});

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should split "Silver Ear,Wood Ear,Oyster"', async () => {
    const mushrooms = 'Silver Ear,Wood Ear,Oyster';
    const _mushrooms = splitStr(mushrooms, ',');
    expect(_mushrooms).toEqual(['Silver Ear', 'Wood Ear', 'Oyster']);
  });
});
