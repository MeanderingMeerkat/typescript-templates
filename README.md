# typescript-templates

Simple starter templates for TypeScript.

## ts-express

A mildy opinionated and curated setup for express with TypeScript. Mostly based on Google TypeScript Style (gts) and the standard express-generator template.

## ts-cli

A simple oclif TypeScript template for command line tools
