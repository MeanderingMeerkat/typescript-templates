import * as path from 'path';
import createError from 'http-errors';
import express, {ErrorRequestHandler, Express} from 'express';
import cors from 'cors';
import helmet from 'helmet';
import logger from 'morgan';

import {IAppCfg} from './interfaces/cfg';

import {indexRouter} from './routes/index';
import {healthRouter} from './routes/health';
import {conversionRouter} from './routes/convert-units';

export function buildApp(cfg: IAppCfg): Express {
  const app = express();
  app.set('env', cfg.env);

  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  app.use(logger(cfg.env === 'development' ? 'dev' : 'tiny'));
  app.use(express.json());
  app.use(cors());
  app.use(helmet());
  app.use(express.urlencoded({extended: false}));
  app.use(express.static(path.join(__dirname, 'public')));

  // Routes and handlers
  app.use('/', indexRouter);
  app.use('/', healthRouter);
  app.use('/convert', conversionRouter);

  // If no route resolved, forward to error handler
  app.use((req, res, next) => {
    next(createError(404));
  });
  const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = cfg.env === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  };
  app.use(errorHandler);

  return app;
}
