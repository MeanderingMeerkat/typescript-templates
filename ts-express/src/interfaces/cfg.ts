export type IAppCfg = {
  port: string;
  env: 'development' | 'production';
};
