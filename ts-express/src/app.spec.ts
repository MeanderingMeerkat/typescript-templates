import {Express} from 'express';
import request from 'supertest';

import {buildApp} from './app';

describe('Units Convertor', () => {
  let app: Express;

  beforeAll(async () => {
    app = buildApp({
      port: '3000',
      env: 'development',
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should respond to health checks', async () => {
    const resp = await request(app).get('/health');
    expect(resp.statusCode).toEqual(200);
  });
});
