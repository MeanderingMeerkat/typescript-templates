import {convertor} from './convertor';

describe('Units Convertor', () => {
  beforeAll(async () => {});

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should convert degC to degF', async () => {
    const degC = -50;
    const degF = convertor(degC, 'degC->degF');
    expect(degF - -58).toBeLessThan(1e-5);
  });

  it('Should convert degF to degC', async () => {
    const degF = 28.4;
    const degC = convertor(degF, 'degF->degC');
    expect(degC - -2).toBeLessThan(1e-5);
  });
});
