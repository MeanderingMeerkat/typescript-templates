import {z} from 'zod';

export const supportedConversions = z.union([
  z.literal('degC->degF'),
  z.literal('degF->degC'),
]);

export const convertor = (
  value: number,
  conversion: z.infer<typeof supportedConversions>
): number => {
  switch (conversion) {
    case 'degC->degF':
      return (value * 9) / 5 + 32;
    case 'degF->degC':
      return ((value - 32) * 5) / 9;
    default:
      throw new Error('Conversion not supported');
  }
};
