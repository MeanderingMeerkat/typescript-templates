import {buildApp} from './app';
import {appCfg} from './cfg';

// ------- ENTRY POINT --------
(async (): Promise<void> => {
  const cfg = appCfg();
  const app = buildApp(cfg);
  const server = app.listen(cfg.port);
  server.on('listening', () => {
    const addr = server.address();
    const bind =
      typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr!.port;
    console.info('Listening on ' + bind);
  });
})().catch(err => console.error(err));
