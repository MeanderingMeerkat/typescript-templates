import express, {
  NextFunction,
  Request,
  RequestHandler,
  Response,
  Router,
} from 'express';

const router = express.Router();

const indexHandler: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.render('index', {title: 'Express'});
};

export const indexRouter: Router = router.get('/', indexHandler);
