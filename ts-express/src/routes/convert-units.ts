import express, {
  NextFunction,
  Request,
  RequestHandler,
  Response,
  Router,
} from 'express';
import {z} from 'zod';

import {supportedConversions, convertor} from '../services/convertor';

const router = express.Router();

const sampleParser = z.object({
  samples: z.array(
    z.object({
      value: z.number(),
      conversion: supportedConversions,
    })
  ),
});

const conversionHandler: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const samplesToConvert = sampleParser.parse(req.body).samples;
  const convertedSamples = samplesToConvert.map(sample =>
    convertor(sample.value, sample.conversion)
  );
  res.status(200).send(JSON.stringify({samples: convertedSamples}));
};

export const conversionRouter: Router = router.post('/', conversionHandler);
