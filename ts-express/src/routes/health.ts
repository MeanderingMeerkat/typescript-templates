import express, {
  NextFunction,
  Request,
  RequestHandler,
  Response,
  Router,
} from 'express';

const router = express.Router();

const healthCheckHandler: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.sendStatus(200);
};

export const healthRouter: Router = router.get('/health', healthCheckHandler);
