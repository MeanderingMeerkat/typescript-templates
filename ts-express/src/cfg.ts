import dotenv from 'dotenv';

import {IAppCfg} from './interfaces/cfg';

export const appCfg = (): IAppCfg => {
  dotenv.config();
  return {
    port: process.env['PORT'] || '3000',
    env: process.env['ENV'] === 'development' ? 'development' : 'production',
  };
};
